Imports System.Windows.Forms


Public Class clsErr
    Dim objErr As Exception
    Shared _clserr As clsErr = New clsErr

    ''' <summary>
    ''' インスタンス作成
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function GetInst() As clsErr
        Return _clserr
    End Function

    ''' <summary>
    ''' エラー処理
    ''' </summary>
    ''' <param name="err">エラーオブジェクト</param>
    ''' <remarks></remarks>
    Public Sub ErrProc(ByVal err As Exception)
        objErr = err

        ErrMsg(objErr)
        OutputLog(objErr)
        
    End Sub

    Private Sub ErrMsg(ByVal err As Exception)
        MessageBox.Show(err.Message, "報告", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
    End Sub

    Private Sub OutputLog(ByVal err As Exception)
        Dim sw As New IO.StreamWriter(Application.StartupPath & "\log.txt", True, Text.Encoding.GetEncoding("Shift-JIS"))
        Dim strlog As String = String.Empty
        strlog &= Now.ToString("yyyy/MM/dd HH:mm:ss") & Space(1) & err.Message & Space(1)
        If Not IsNothing(err.InnerException) Then
            strlog &= err.InnerException.ToString
        End If


        sw.WriteLine(strlog)
        sw.Close()
    End Sub

    Private Sub New()

    End Sub
End Class
